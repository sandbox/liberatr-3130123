<?php

/**
 * @file
 * Definition of import batch created on fetching stage by OAuth2ClientFetcher.
 */

/**
 * OAuth2ClientFetcherResult.
 *
 * Extends FeedsFetcherResult.
 */
class OAuth2ClientFetcherResult extends FeedsHTTPFetcherResult {
  /**
   * The authentication flow that provides the needed OAuth tokens.
   *
   * @var string
   */
  protected $authFlow;
  /**
   * The Consumer Key.
   *
   * @var string
   */
  protected $consumerKey;
  /**
   * The Consumer Secret to match the Key.
   *
   * @var string
   */
  protected $consumerSecret;
  /**
   * Used to identify the current feeds instance.
   *
   * @var string
   */
  protected $id;
  /**
   * Used for identifying feed in logs.
   *
   * @var string
   */
  protected $siteId;
  /**
   * HTTP Method - POST or GET.
   *
   * @var string
   */
  protected $method;
  /**
   * User ID to associate imported content.
   *
   * @var string
   */
  protected $uid;

  /**
   * Constructor.
   */
  public function __construct($url, $authFlow, $consumerKey, $consumerSecret, $id, $siteId, $method, $uid) {
    $this->authFlow = $authFlow;
    $this->consumerKey = $consumerKey;
    $this->consumerSecret = $consumerSecret;
    $this->id = $id;
    $this->siteId = $siteId;
    $this->method = $method;
    $this->uid = $uid;
    parent::__construct($url);
  }

  /**
   * Implementation of FeedsImportBatch::getRaw().
   */
  public function getRaw() {
    // Get access token.
    $access_token = call_user_func('feeds_oauth2client_get_tokens', $this->uid, $this->siteId, $this->id);
    if (empty($access_token['oauth_token'])) {
      watchdog('feeds_oauth', 'Authenticator %authenticator returned empty access token for uid %uid, site %site. Aborting.', array(
        '%authenticator' => 'feeds_oauth2client_get_tokens',
        '%uid' => $this->uid,
        '%site' => $this->siteId,
      ), WATCHDOG_WARNING);
      return array();
    }

    // Include HTTP functions.
    feeds_include_library('http_request.inc', 'http_request');

    // Try to fetch the data from a URL.
    $result = feeds_http_request($this->url, array(
      'accept_invalid_cert' => $this->acceptInvalidCert,
      'timeout' => $this->timeout,
      'cache_http_result' => $this->cacheHttpResult,
      'bearer' => $access_token['oauth_token'],
    ));
    http_request_check_result($this->url, $result);
    $this->raw = $result->data;

    return $this->sanitizeRawOptimized($this->raw);
  }

}

/**
 * Support OAuth authentication.
 */
class OAuth2ClientFetcher extends FeedsHTTPFetcher {

  /**
   * {@inheritdoc}
   */
  public function __construct($url = NULL) {
    parent::__construct($url);
  }

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    return new OAuth2ClientFetcherResult(
      trim($source_config['source']),
      $this->config['auth_flow'],
      $this->config['consumerKey'],
      $this->config['consumerSecret'],
      $this->id,
      $this->config['site_id'],
      $this->config['method'],
      $this->getAuthenticatedUser($source)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function configDefaults() {
    global $user;
    return array(
      'auth_flow' => 'client-credentials',
      'site_id' => '',
      'consumerKey' => '',
      'consumerSecret' => '',
      'access_token_url' => '',
      'authorize_url' => '',
      'method' => 'post',
      'uid' => $user->uid,
    ) + parent::configDefaults();
  }

  /**
   * {@inheritdoc}
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    $form['use_pubsubhubbub'] = array('#type' => 'value', '#value' => FALSE);
    $form['auth_flow'] = array(
      '#type' => 'select',
      '#title' => t('OAuth auth_flow'),
      '#default_value' => $this->config['auth_flow'],
      '#options' => array(
        'client-credentials' => 'client-credentials',
      ),
      '#description' => t('Choose the authentication method that provides the needed OAuth tokens.'),
    );
    $form['uid'] = array(
      '#type' => 'select',
      '#title' => t('Authenticating user'),
      '#description' => t('OAuth access tokens will be retrieved for this user when fetching the feed.
                           Select "- None -" to use the source node\'s owner (in the case of using a source node)
                           or the current user (in the case of standalone source form).'
      ),
      '#default_value' => $this->config['uid'],
      '#options' => array(NULL => t('- None -')) + db_query("SELECT uid, name FROM {users} WHERE status = 1 and uid != 0")->fetchAllKeyed(),
    );
    $form['site_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Site identifier'),
      '#default_value' => $this->config['site_id'],
      '#description' => t('Internal identifier for this connection. Should only contain alphanumeric characters and hyphens. For the current configuration, callback URL will be: <code class="site-id">%url</code>',
        array('%url' => url('feeds/oauth' . ($this->two ? '2' : '') . '/callback/' . $this->config['site_id'], array('absolute' => TRUE)))
      ),
      '#required' => TRUE,
      '#attached' => array(
        'js' => array(
          drupal_get_path('module', 'feeds_oauth') . '/feeds_oauth.js',
        ),
      ),
    );
    $form['consumerKey'] = array(
      '#type' => 'textfield',
      '#title' => t('Consumer key'),
      '#default_value' => $this->config['consumerKey'],
      '#required' => TRUE,
    );
    $form['consumerSecret'] = array(
      '#type' => 'textfield',
      '#title' => t('Consumer secret'),
      '#default_value' => $this->config['consumerSecret'],
      '#required' => TRUE,
    );
    $form['access_token_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Access token URL'),
      '#default_value' => $this->config['access_token_url'],
      '#required' => TRUE,
    );
    $form['authorize_url'] = array(
      '#type' => 'hidden',
      '#title' => t('Authorize URL'),
      '#default_value' => $this->config['authorize_url'],
      '#required' => FALSE,
    );
    $form['method'] = array(
      '#type' => 'select',
      '#title' => t('Method'),
      '#default_value' => $this->config['method'],
      '#options' => array('get' => 'GET', 'post' => 'POST'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configFormValidate(&$values) {
    $values['site_id'] = trim($values['site_id']);
    if (!preg_match('/^[\w-]*$/', $values['site_id'])) {
      form_set_error('site_id', t('Site identifier must contain alphanumerics and hyphens only.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getAuthenticatedUser($source) {
    global $user;
    $source_node = node_load(@$source->feed_nid);
    return !empty($this->config['uid']) ? $this->config['uid'] : (
      $source_node ? $source_node->uid : $user->uid
    );
  }

  /**
   * {@inheritdoc}
   */
  public function sourceForm($source_config) {
    $form = parent::sourceForm($source_config);
    if (empty($source_config)) {
      return $form;
    }

    // FIXME Ugly hack to get caller FeedsSource object. We know a FeedsSource object will call this method.
    $trace = debug_backtrace();
    $source = $trace[1]['object'];

    // Check existence of access tokens.
    $uid = $this->getAuthenticatedUser($source);
    $access_tokens = call_user_func('feeds_oauth2client_get_tokens', $uid, $this->config['site_id'], $this->id);
    if (empty($access_tokens['oauth_token']) && !empty($this->config['site_id'])) {
      global $user;
      // User can request new access token.
      if ($uid === $user->uid) {
        drupal_set_message(t('Could not find OAuth access tokens for site %site.
          You should <a href="@url">authenticate first</a> to access protected information.',
          array('%site' => $this->config['site_id'], '@url' => url('feeds/oauth2client/get_token/' . $this->id))
        ), 'warning');
      }
      // Another user was selected, issue warning.
      else {
        $authenticating_user = user_load($uid);
        drupal_set_message(t('Could not find OAuth access tokens for site %site.
          In addition, the authenticating user for this feed is set to be %username,
          so you cannot import this feed until %username authenticates to %site.',
          array('%site' => $this->config['site_id'], '%username' => $authenticating_user->name)
        ), 'warning');
      }
    }
    return $form;
  }

}
