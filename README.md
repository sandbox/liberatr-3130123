# Feeds Oauth2 Client

This module creates a way of fetching a feed using the Feeds HTTP fetcher and authenticating using Oauth 2 bearer tokens. At this time, a patch is required in order for the fetcher to work.

See [this issue](https://www.drupal.org/project/feeds/issues/3064472#comment-13567567) for the patch.

To use, create a Feed, change the Fetcher to "OAuth 2.0 Client Credentials fetcher" then add an identifier, token and URL for your feed.

The first time you access the feed, you need to get your bearer token, then the module should automatically grab a new token each time your old one expires.
